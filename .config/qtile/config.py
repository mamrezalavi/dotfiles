#   Mamiza's ~/.config/qtile/config.py

# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

# Setting mod as "Super" and termianl as "alacritty"
mod = "mod4"
terminal = "alacritty"

# Some keybindings
keys = [
    # Switch between windows
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus right"),
    Key([mod], "Tab", lazy.layout.next(), desc="Move window focus to other window"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "j", lazy.layout.down(), desc="Move foucs down"),
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),

    # Switch fullscreen mode
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Fullscreen the focused window"),

    # Move windows
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),

    # Resize windows
    Key([mod, "shift"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod, "shift"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "shift"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "shift"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),

    # Reset window size
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Launch terminal
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Switch layouts
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),

    # Kill window
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    # Restart qtile
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),

    # Shutdown qtile
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile")
    # Key([mod], "r", lazy.spawncmd(),
        # desc="Spawn a command using a prompt widget"),
]

# Initiate groups
def init_groups():
    groups = []
    # group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
    # group_labels = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
    group_names = ["1", "2", "3", "4", "5", "6"]
    group_labels = ["WEB", "DEV", "VBOX", "VID", "DUM", "BG"]
    group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", ]

    for i in range(len(group_names)):
        groups.append(
            Group(
                name = group_names[i],
                label = group_labels[i],
                layout = group_layouts[i]
            )
        )
    return groups

groups = init_groups()

# Some more keybindings
for i in groups:
    keys.extend([
        # Change workspace
        Key([mod], i.name, lazy.group[i.name].toscreen(), desc="Switch to group {}".format(i.name)),

        # mod + shift + letter of group = switch to & move focused window to group
        Key([mod, "control"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # mod + shift + letter of group = move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            desc="move focused window to group {}".format(i.name)),
    ])

# Initiate theme for layouts
def init_layout_theme():
    return {
        "margin" : 8,
        "border_width" : 2,
        "border_focus" : "#5e81ac",
        "border_normal" : "#4c566a"
    }
layout_theme = init_layout_theme()

layouts = [
    layout.MonadTall(
        **layout_theme,
        ratio = 0.55,
        name = "MONADTALL"
    ),
    layout.Max(
        **layout_theme
    )
    # Try more layouts by unleashing below layouts.
    # layout.Columns(border_focus_stack='#d75f5f'),
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

# Initiate colors
def init_colors():
    return [["#282c34", "#282c34"], # color 0 -- panel background -- Dark Grey
            ["#ffffff", "#ffffff"], # color 1 -- panel foreground -- White
            ["#d13ca4", "#d13ca4"], # color 2 -- background for focused group -- Magenta
            ["#808080", "#808080"], # color 3 -- seperator line color -- Grey
            ["#ff4444", "#ff4444"], # color 4 -- foreground for current layout widget -- Red
            ["#01fdb0", "#01fdb0"], # color 5 -- foreground for cpu widget -- Mint
            ["#ffff44", "#ffff44"], # color 6 -- foreground for memory widget -- Yellow
            ["#00a2ff", "#00a2ff"]] # color 7 -- foreground for clock widget -- Blue

colors = init_colors()

# Initiate default settings for widgets
def init_widgets_default():
    return dict(
        font = "Ubuntu Mono", # Change back to mononoki
        fontsize = 18,
        background = colors[0],
        foreground = colors[1]
    )

widget_defaults = init_widgets_default()

# Initiate widgets
def init_widgets_list():
    widgets_list = [
        widget.Sep(
            padding = 6,
            linewidth = 0,
        ),
        widget.GroupBox(
            margin_x = 0,
            margin_y = 3,
            border_width = 0,
            active = colors[1],
            inactive = colors[1],
            this_current_screen_border = colors[2],
            highlight_method  = "block",
            disable_drag = True,
            rounded = False
        ),
        widget.Sep(
            padding = 10,
            linewidth = 2,
            foreground = colors[3]
        ),
        widget.WindowName(
            font = "Ubuntu Mono", # Change back to Mononoki
            fontsize = 16
        ),
        widget.Systray(
            padding = 10,
            icon_size = 22
        ),
        widget.Sep(
            padding = 8,
            linewidth = 0,
        ),
        widget.Sep(
            padding = 10,
            linewidth = 2,
            foreground = colors[3]
        ),
        widget.CurrentLayout(
            font = "Ubuntu Mono", # Change back to Mononoki
            fontsize = 18 # Change back to 18 after changing the font
        ),
        widget.Sep(
            padding = 10,
            linewidth = 2,
            foreground = colors[3]
        ),
        widget.Battery(
            font = "Ubuntu Mono", # Change back to Mononoki
            fontsize = 18, # Change back to 18 after changing the font
            full_char = "Full",
            charge_char = "Charging",
            discharge_char = "Discharging",
            unknown_char = "Unknown",
            empty_char = "Too late ;)",
            update_interval = 5,
            format = "{char} {percent:2.0%} {hour}:{min}",
            foreground = colors[2]
        ),
        widget.Sep(
            padding = 10,
            linewidth = 2,
            foreground = colors[3]
        ),
        widget.CPU(
            font = "Ubuntu Mono", # Change back to Mononoki
            fontsize = 18, # Change back to 18 after changing the font
            foreground = colors[5]
        ),
        widget.Sep(
            padding = 10,
            linewidth = 2,
            foreground = colors[3]
        ),
        widget.Memory(
            font = "Ubuntu Mono", # Change back to Mononoki
            fontsize = 18, # Change back to 18 after changing the font
            foreground = colors[6]
        ),
        widget.Sep(
            padding = 10,
            linewidth = 2,
            foreground = colors[3]
        ),
        widget.Clock(
            font = "Ubuntu Mono", # Change back to Mononoki
            fontsize = 18, # Change back to 18 after changing the font
            foreground = colors[7],
            format = "%A %B %d - %H:%M"
        ),
        widget.Sep(
            padding = 10,
            linewidth = 2,
            foreground = colors[3]
        ),
        widget.TextBox(
            text = "Q",
            fontsize = 18, # Change back to 18 after changing the font
            foreground = colors[4]
        )
    ]
    return widgets_list

screens = [Screen(top=bar.Bar(widgets=init_widgets_list(), size=20, opacity=1.0))]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

bring_front_click = True
floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class='confirmreset'),
        Match(wm_class='makebranch'),
        Match(wm_class='maketag'),
        Match(wm_class='ssh-askpass'),
        Match(title='branchdialog'),
        Match(title='pinentry')
    ],
    **layout_theme
)

@hook.subscribe.startup_once
def startup_once():
    home = os.path.expanduser('~')
    subprocess.call([home + "/.config/qtile/startup_once.sh"])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

# main = None  # WARNING: this is deprecated and will be removed soon --> uncomment if anything goes wrong
