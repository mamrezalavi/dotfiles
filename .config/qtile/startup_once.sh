#!/bin/bash

picom &
lxsession &
nitrogen --restore &
volumeicon &
nm-applet &
sxhkd -c ~/.config/sxhkd/sxhkdrc
