#   Mamiza's "~/.bashrc"

### Exports
export EDITOR="vim"

# Set the manpager (default manpager is less)
# "vim" as manpager
export MANPAGER='/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'

### If not running interactively, don't do anything
[[ $- != *i* ]] && return

### Path
if [ -d "$HOME/.bin" ]; then
    PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi

### Shopt
shopt -s histappend
shopt -s cmdhist

### Extraction
ex ()
{
    if [ -f $1 ]; then
        case $1 in
            *tar.bz2)   tar xjf $1 ;;
            *.tbz2)     tar xjf $1 ;;
            *.tar.gz)   tar xzf $1 ;;
            *.tgz)      tar xzf $1 ;;
            *.bz2)      bunzip2 $1 ;;
            *.gz)       gunzip  $1 ;;
            *.tar)      tar xf  $1 ;;
            *.tar.zst)  unzstd  $1 ;;
            *)          echo "\"$1\" cannot be extracted via ex" ;;
        esac
    else
        echo "\"$1\" is not a valid file"
    fi
}

### Aliases
# cd
alias ..="cd .."
alias ...="cd ..."

# vim
alias vi="vim"

# ls
alias ls="exa -la --color=always --group-directories-first"
alias l.="exa -a | egrep \"^\.\""

# pacman
alias pacman="pacman --color auto"
alias pacsyu="sudo pacman -Syyu"
alias paccnt="sudo pacman -Q | wc -l"
alias punlock="sudo rm /var/lib/pacman/db.lck"

# grep
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

# cp, mv and rm
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"

# free
alias free="free -m"

# df and du
alias df="df -h"
alias du="du -hs"

# update-grub
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

# fc-cache
alias update-fc="sudo fc-cache -fv"

# shutdown
alias shutdown="shutdown now"

# Merge ~/.Xresources
alias xmerge="xrdb -merge ~/.Xresources"

# youtube-dl
alias ytd="youtube-dl"

### Set the prompt
# PS1='[\u@\h \W]\$ ' # --> the default prompt

# Custom prompt in case starship failed
# Colors
# Red="tput setaf 196"
# Gray="tput setaf 246"
# Blue="tput setaf 12"
# Default_color="tput sgr0"
#
# PS1="\[$($Red)\]["
# PS1+="\[$($Gray)\]\u@\h " # --> \u for username and \h for hostname
# PS1+="\[$($Blue)\]\W" # --> \W for working directory (\w for full path to working directory)
# PS1+="\[$($Red)\]]"
# PS1+="\[$($Default_color)\]: "

eval "$(starship init bash)"
