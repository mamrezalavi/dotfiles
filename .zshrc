#   Mamiza's ~/.zshrc

### Autoloads
autoload -U colors && colors
autoload -U compinit

### Keybindings
# Vim keybindings
bindkey -v
export KEYTIMEOUT=1

bindkey ^A beginning-of-line

### Cursor shape
# Different cursor shapes for different vim modes
function zle-keymap-select {
    if [[ ${KEYMAP} == vicmd ]] || [[ $1 = "block" ]]; then
        echo -ne "\e[1 q" # --> Use block in cmd mode
    elif [[ ${KEYMAP} == main ]] || [[ ${KEYMAP} == viins ]] ||
         [[ ${KEYMAP} == '' ]]   || [[ $1 == "beam" ]]; then
        echo -ne "\e[5 q" # --> Use beam in ins mode
    fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne "\e[5 q"
preexec() { echo -ne "\e[5 q" ;} # Use beam for each new prompt

### Exports
export EDITOR="vim"

# Set the manpager (default manpager is less)
# "vim" as manpager
export MANPAGER='/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'

### Path
if [ -d "$HOME/.bin" ]; then
    PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi

### History
HISTSIZE=
SAVEHIST=
HISTFILE=~/.zsh_history

function ex {
    if [ -z $1 ]; then
        echo "Usage: ex <path/file_name>.<tar.bz2|tbz2|bz2|tar.gz|tgz|gz|tar|tar.zst"
        echo "       ex <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]..."
    else
        for n in "$@"; do
            if [ -f "$n" ]; then
                case "${n%,}" in
                    *.cbt | *.tar.bz2 | *.tar.gz | *.tar.xz | *.tbz2 | *.tgz | *.txz | .tar)
                        tar xvf "$n"    ;;
                    *.bz2)  bunzip ./"$n" ;;
                    *.gz)   gunzip ./"$n" ;;
                    *.z)    uncompress ./"$n" ;;
                    *)  echo "ex: '$n' - unknown archive method" && return 1 ;;
                esac
            else
                echo "'$n' - file does not exist" && return 1
            fi
        done
    fi
}

### Aliases
# cd
alias ..="cd .."
alias ...="cd ..."

# vim
alias vi="vim"

# ls
alias ls="exa -la --color=always --group-directories-first"
alias l.="exa -a | egrep \"^\.\""

# pacman
alias pacman="pacman --color auto"
alias pacsyu="sudo pacman -Syyu"
alias paccnt="sudo pacman -Q | wc -l"
alias punlock="sudo rm /var/lib/pacman/db.lck"

# grep
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

# cp, mv and rm
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"

# free
alias free="free -m"

# df and du
alias df="df -h"
alias du="du -hs"

# update-grub
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

# fc-cache
alias update-fc="sudo fc-cache -fv"

# shutdown
alias shutdown="shutdown now"

# Merge ~/.Xresources
alias xmerge="xrdb -merge ~/.Xresources"

# youtube-dl
alias ytd="youtube-dl"

### Tab completion
zstyle ":completion:*" menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # --> Include dotfiles

### Set the prompt
# PS1="%M%%" # --> The default prompt

# Custom prompt in case starship failed
PS1="%B%{$fg[red]%}["
PS1+="%{$fg[yellow]%}%n" # --> %n for username
PS1+="%{$fg[green]%}@"
PS1+="%{$fg[blue]%}%M "  # --> %M for hostname
PS1+="%{$fg[magenta]%}%c" # --> %c for working directory (use '~' for full path)
PS1+="%{$fg[red]%}]"
PS1+="%{$reset_color%}: "

eval "$(starship init zsh)"

### Syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
